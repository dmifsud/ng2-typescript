

class DivineObject{
  constructor(public name: String, public surname: String){
    console.log("created object");
  }

  public printName() : string{
    return (this.name + " " + this.surname);
  };
};

let me: DivineObject = new DivineObject("David", "Mifsud");

console.log(me.printName());
