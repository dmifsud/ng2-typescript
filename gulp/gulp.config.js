module.exports = function(){
  'use strict';
  var config = {
    allTs: './src/**/*.ts',
    allHtml: './src/**/*.html',
    output: './build',
    allOutput: './build/**/*'
  };

  return config;
};
