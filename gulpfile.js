var gulp = require('gulp');

require('requiredir')('./gulp');

var ts = require('gulp-typescript');
var tslint = require('gulp-tslint');
var sourcemaps = require('gulp-sourcemaps');
var runSequence = require('run-sequence');
var tsProject = ts.createProject('tsconfig.json');
var config = require('./gulp/gulp.config')();
var tsConfig = require('./tsconfig.json');
var browserSync = require('browser-sync');
var inject = require('gulp-inject');
var del = require('del');
var superstatic = require('superstatic');

gulp.task('clean', function(){
  return del(config.allOutput);
});

gulp.task('index', function(){
  var target = gulp.src('./src/index.html');
  // It's not necessary to read the files (will speed up things), we're only after their paths:
  //var sources = gulp.src(['./build/app/app.js', './src/**/*.css'], {read: false}, {relative: true});
  //https://github.com/klei/gulp-inject
  return target.pipe(inject(gulp.src(['build/app/**/*.js', './build/**/*.css'], {read: false})))
          .pipe(inject(gulp.src(['build/lib/**/*.js'], {read: false}), {name: 'head'}))
          .pipe(gulp.dest(config.output));
});

gulp.task('lib', function(){


  return gulp.src([
      'node_modules/angular2/bundles/angular2-polyfills.js',
      'node_modules/systemjs/dist/system.src.js',
      'node_modules/@reactivex/rxjs/dist/global/Rx.js',
      'node_modules/angular2/bundles/angular2.dev.js',
      'node_modules/angular2/bundles/router.dev.js'
    ])
    .pipe(gulp.dest(config.output + "/lib"));

});



//******************************************************************************
//                          COMPILE TYPESCRIPT
//******************************************************************************

gulp.task('ts-lint', function(){
  return gulp.src(config.allTs)
    .pipe(tslint())
    .pipe(tslint.report('prose', {
      emitError: false
    }));
});

gulp.task('compile-ts', /*['clean'],*/ function(){
  var tsSourceFiles = [
    config.allTs
    //check for typings http://stackoverflow.com/questions/12687779/how-do-you-produce-a-d-ts-typings-definition-file-from-an-existing-javascript
  ];

  var tsResult = gulp.src(tsSourceFiles)
    .pipe(sourcemaps.init())
    .pipe(ts(tsProject));

  return tsResult.js
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest(config.output));
});

gulp.task('watch', function(){
  return gulp.watch([config.allTs, config.allHtml], ['ts-lint', 'compile-ts', 'index']);
});

gulp.task('build', function(callback){

  runSequence('clean', 'ts-lint', ['compile-ts', 'lib'], 'index', 'watch',
              callback);


});

gulp.task('serve', ['build'], function(){

  browserSync({
    port: 4000,
    file: ['index.html', 'app/app.js'],
    injectChanges: true,
    logFileChanges: false,
    logLevel: 'silent',
    notify: true,
    reloadDelay: 0,
    server: {
      baseDir: './build',
      middleware: superstatic({debug: false})
    }
  });

  console.log('Connected to: http://localhost:4000');
});



gulp.task('default', function(cb){

});
